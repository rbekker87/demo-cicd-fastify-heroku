# demo-cicd-restify-heroku

CI/CD Demo with Restify, Gitlab-CI and Heroku

## Resources:

- https://willi.am/blog/2014/07/28/test-your-api-with-supertest/
- https://www.codementor.io/knownasilya/testing-express-apis-with-supertest-du107mcv2
- https://puigcerber.com/2015/11/27/testing-express-apis-with-tape-and-supertest/
