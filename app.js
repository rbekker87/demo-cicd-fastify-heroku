var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./routes/users.js');
var app = express();

app.disable('x-powered-by');
app.disable('etag');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

routes(app);

//var server = app.listen(3000, function() {
//  console.log('app running on port', server.address().port);
//});

app.listen(process.env.PORT || 3000, console.log('app running on port 3000'));

module.exports = app;
