var request = require('supertest');
var test = require('tape');
var app = require('../app.js');

// test root endpoint
describe('GET /', function() {
  it('respond with 200 status code and message', function(done) {
    request(app)
      .get('/')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect({ message: 'Restful API' }, done);
  });
});

// test user endpoint
describe('GET /user', function() {
  it('/user endpoint: respond with 200 status code and user', function(done) {
    request(app)
      .get('/user')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});

// test users endpoint
describe('GET /users', function() {
  it('/users endpoint: respond with 200 status code and user', function(done) {
    request(app)
      .get('/users/4')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});


