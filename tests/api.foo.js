var request = require('supertest');
var start = require('../index.js');

describe('GET /', function() {
  it('respond with 200', function(done) {
    request(start)
	.put('/a/doo')
	.set('Accept', 'application/json')
	.expect(200)
	.end(done());
  });
});
