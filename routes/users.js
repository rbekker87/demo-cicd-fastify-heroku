var faker = require('faker');

var randomnames = ['ruan', 'james', 'frank'];

var approuter = function(app) {
  app.get('/', function(req, res) {
    res.set('Content-Type', 'application/json');
    res.status(200).json({ message: 'Restful API' });
  });

  app.get('/random', function(req, res) {
    res.set('Content-Type', 'application/json');
    res.json(randomnames);
  });

  app.get('/user', function(req, res) {
    var data = ({
      firstname: faker.name.firstName(),
      lastname: faker.name.lastName(),
      username: faker.internet.userName(),
      email: faker.internet.email(),
      version: '1.0.0'
    });
    res.set('Content-Type', 'application/json');
    res.status(200).send(data);
  });

  app.get('/users/:num', function(req, res) {
    var users = [];
    var num = req.params.num;
    
    if (isFinite(num) && num > 0) {
      for (i = 0; i <= num-1; i++) {
        users.push({
          firstname: faker.name.firstName(),
          lastname: faker.name.lastName(),
          username: faker.internet.userName(),
          email: faker.internet.email(),
          version: '1.0.0'
        });
      }
      res.set('Content-Type', 'application/json');
      res.status(200).send(users);

    } else {
      res.status(400).send({ message: 'invalid number supplied' });
    }

  });
}

module.exports = approuter;
